var fbgraph = require('co-fbgraph')
  , koa     = require('koa')
  , route   = require('koa-route')
  , routes  = require(__dirname + '/routes')
  ;  

var app = koa();

app.use(route.get('/', routes.main));
app.use(route.get('/auth/facebook', routes.facebookAuth));
app.use(route.get('/loginSuccess', routes.loginSuccess));

app.listen(3000);
console.log('Listening on port 3000');
