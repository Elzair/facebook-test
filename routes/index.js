var fs    = require('co-fs')
  , graph = require('co-fbgraph');

var main = exports.main = function*(next) {
  this.body = yield fs.readFile(__dirname + '/../public/index.html', 'utf8');
  yield next;
};

var facebookAuth = exports.facebookAuth = function*(next) {
  var auth = require(__dirname + '/../conf/auth');

  if (!this.query.code) {
    var authUrl = graph.getOauthUrl({
        "client_id":    auth.client_id
      , "redirect_uri": auth.redirect_uri
      , "scope":        auth.scope
    });
    console.log(authUrl);

    if (!this.query.error) {
      this.redirect(authUrl);
    }
    else {
      this.body = 'Access Denied';
    }
  }
  else {
    yield graph.authorize({
        "client_id":    auth.client_id
      , "redirect_uri": auth.redirect_uri
      , "scope":        auth.scope
      , "code":         this.query.code
    });
  }

  yield next;
};

var loginSuccess = exports.loginSuccess = function*(next) {
  
  if (!this.query.code) {
    this.body = 'You have not logged in yet!';
  }
  else {
    graph.setAccessToken(this.query.code);

    var wallPost = {
      message: "This is a test of the Facebook API. This is only a test. If this were the real Facebook API, I would be a billionaire."
    };

    try {
      var res = yield graph.post('/feed', wallPost);
      this.body = 'You have successfully logged in!';
    }
    catch (err) {
      this.body = err;
    }
  }

  yield next;
};
